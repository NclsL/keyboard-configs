# DZ60
A customizable 60% keyboard.

Keyboard Maintainer: QMK Community  
Hardware Supported: DZ60  

Nordic layout with the following keys
![alt text](https://i.imgur.com/SK3V1e0.png "Nordic ISO dz60 with arrows")

See keymaps/nordicISO_plus_arrows/keymap.c for customization

Make example for the nordic keyboard (after setting up your build environmen):

    make dz60:nordicISO_plus_arrows

See https://docs.qmk.fm/qmk-introduction for more information.
